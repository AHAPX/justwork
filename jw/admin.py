from django.contrib import admin
from django.contrib.contenttypes.admin import GenericTabularInline

from jw import models


class ContentInline(GenericTabularInline):
    model = models.PageContentItem


class Content4PageInline(admin.TabularInline):
    model = models.PageContentItem


@admin.register(models.Page)
class CoinAdmin(admin.ModelAdmin):
    list_display = ('title',)
    order = ('title',)
    inlines = [Content4PageInline]


@admin.register(models.ContentVideo)
class ContentVideoAdmin(admin.ModelAdmin):
    list_display = ('title', 'counter')
    order = ('title',)
    inlines = [ContentInline]


@admin.register(models.ContentAudio)
class ContentAudioAdmin(admin.ModelAdmin):
    list_display = ('title', 'counter')
    order = ('title',)
    inlines = [ContentInline]


@admin.register(models.ContentText)
class ContentTextAdmin(admin.ModelAdmin):
    list_display = ('title', 'counter')
    order = ('title',)
    inlines = [ContentInline]


@admin.register(models.PageContentItem)
class ContentAdmin(admin.ModelAdmin):
    list_display = ('page', 'content_type', 'content_object')
