from django.db import models
from django.db.models import Q
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType


class Page(models.Model):
    title = models.CharField(max_length=30)

    class Meta:
        ordering = ['id']

    def __str__(self):
        return self.title


class BaseContent(models.Model):
    title = models.CharField(max_length=30)
    counter = models.PositiveIntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        abstract = True

    def __str__(self):
        return self.title

    def get_type(self):
        raise NotImplemented


class ContentVideo(BaseContent):
    content_type = 'video'

    url_file = models.URLField()
    url_subtitles = models.URLField()


class ContentAudio(BaseContent):
    content_type = 'audio'

    url_file = models.URLField()
    bitrate = models.IntegerField()


class ContentText(BaseContent):
    content_type = 'text'

    text = models.TextField()


class PageContentItem(models.Model):
    page = models.ForeignKey(Page, related_name='contents', on_delete=models.CASCADE)
    content_type = models.ForeignKey(
        ContentType,
        on_delete=models.CASCADE,
        limit_choices_to=(
            Q(app_label='jw', model='contentvideo') |
            Q(app_label='jw', model='contentaudio') |
            Q(app_label='jw', model='contenttext')
        )
    )
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')
    order_number = models.IntegerField(default=0)

    def __str__(self):
        return f'{self.page}: {self.content_object}'
