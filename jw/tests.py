from django.contrib.contenttypes.models import ContentType
from django.test import TestCase
from django.urls import reverse
from rest_framework.test import RequestsClient
from rest_framework.test import APITestCase

from jw.celery import app
from jw.models import Page, ContentVideo, ContentAudio, ContentText, PageContentItem


class TestPagesView(APITestCase):
    def setUp(self):
        self.page1 = Page.objects.create(title='page1')
        self.page2 = Page.objects.create(title='page2')

    def test_get(self):
        resp = self.client.get(reverse('page-list'))
        expected = {
            'count': 2,
            'next': None,
            'previous': None,
            'results': [{
                'title': self.page1.title,
                'url': f'http://testserver/api/pages/{self.page1.id}/',
            }, {
                'title': self.page2.title,
                'url': f'http://testserver/api/pages/{self.page2.id}/',
            }],
        }
        self.assertEqual(resp.json(), expected)


class TestPageView(APITestCase):
    def setUp(self):
        app.conf.update(CELERY_ALWAYS_EAGER=True)
        self.page1 = Page.objects.create(title='page1')
        self.page2 = Page.objects.create(title='page2')
        self.video = ContentVideo.objects.create(
            title='video1',
            url_file='https://video.com/file1',
            url_subtitles='https://video.com/subs1'
        )
        self.audio = ContentAudio.objects.create(
            title='audio1',
            url_file='https://audio.com/file1',
            bitrate=256
        )
        self.text = ContentText.objects.create(
            title='text1',
            text='hello'
        )
        PageContentItem.objects.create(
            page=self.page1,
            content_type=ContentType.objects.get_for_model(ContentVideo),
            object_id=self.video.id
        )
        PageContentItem.objects.create(
            page=self.page1,
            content_type=ContentType.objects.get_for_model(ContentAudio),
            object_id=self.audio.id
        )
        PageContentItem.objects.create(
            page=self.page2,
            content_type=ContentType.objects.get_for_model(ContentVideo),
            object_id=self.video.id
        )
        PageContentItem.objects.create(
            page=self.page2,
            content_type=ContentType.objects.get_for_model(ContentText),
            object_id=self.text.id
        )

    def test_get(self):
        # get page1, counters must be 0
        resp = self.client.get(reverse('page-detail', args=[self.page1.id,]))
        expected = {
            'page': 'page1',
            'content': [{
                'content_type': 'video',
                'title': 'video1',
                'counter': 0,
                'url_file': 'https://video.com/file1',
                'url_subtitles': 'https://video.com/subs1'
            }, {
                'content_type': 'audio',
                'title': 'audio1',
                'counter': 0,
                'url_file': 'https://audio.com/file1',
                'bitrate': 256
            }]
        }
        self.assertEqual(resp.json(), expected)
        # get page2, video counter must be 1, text counter must be 0
        resp = self.client.get(reverse('page-detail', args=[self.page2.id,]))
        expected = {
            'page': 'page2',
            'content': [{
                'content_type': 'video',
                'title': 'video1',
                'counter': 1,
                'url_file': 'https://video.com/file1',
                'url_subtitles': 'https://video.com/subs1'
            }, {
                'content_type': 'text',
                'title': 'text1',
                'counter': 0,
                'text': 'hello'
            }]
        }
        self.assertEqual(resp.json(), expected)
        # get page1, video counter must be 2, audio counter must be 1
        resp = self.client.get(reverse('page-detail', args=[self.page1.id,]))
        expected = {
            'page': 'page1',
            'content': [{
                'content_type': 'video',
                'title': 'video1',
                'counter': 2,
                'url_file': 'https://video.com/file1',
                'url_subtitles': 'https://video.com/subs1'
            }, {
                'content_type': 'audio',
                'title': 'audio1',
                'counter': 1,
                'url_file': 'https://audio.com/file1',
                'bitrate': 256
            }]
        }
        self.assertEqual(resp.json(), expected)
