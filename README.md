# Test task

## Description
Django test task for JustWork

## Requirements
- [python 3.6+](https://www.python.org/download/releases/3.6.0/)
- [docker-compose](https://docs.docker.com/compose/install/)


## Install
```bash
git clone https://gitlab.com/AHAPX/justwork
```

## Usage
```bash
cd justwork/deploy
docker-compose up runserver
```

## API

#### get list of all pages
- url: http://localhost:8000/api/pages/
- method: GET


### get page's data and content
- url: http://localhost:8000/api/pages/<page_id>
- method: GET


## Testing
```bash
cd justwork/deploy
docker-compose up autotests
```