from django.urls import reverse
from rest_framework import serializers

from jw.models import Page, ContentVideo, ContentAudio, ContentText


class PageSerializer(serializers.ModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name='page-detail', lookup_field='pk')

    class Meta:
        model = Page
        fields = ('title', 'url')


class ContentSerializer(serializers.ModelSerializer):
    content_type = serializers.SerializerMethodField()

    def get_content_type(self, obj):
        return obj.content_type


class ContentVideoSerializer(ContentSerializer):
    class Meta:
        model = ContentVideo
        fields = ('content_type', 'title', 'counter', 'url_file', 'url_subtitles')


class ContentAudioSerializer(ContentSerializer):
    class Meta:
        model = ContentAudio
        fields = ('content_type', 'title', 'counter', 'url_file', 'bitrate')


class ContentTextSerializer(ContentSerializer):
    class Meta:
        model = ContentText
        fields = ('content_type', 'title', 'counter', 'text')



content_serializers = {
    ContentVideo: ContentVideoSerializer,
    ContentAudio: ContentAudioSerializer,
    ContentText: ContentTextSerializer,
}
