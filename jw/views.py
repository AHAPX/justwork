from django.http import Http404
from rest_framework import generics
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ReadOnlyModelViewSet

from jw import serializers
from jw.models import Page
from jw.tasks import counter_inc


class PagesView(generics.ListAPIView):
    queryset = Page.objects.all()
    serializer_class = serializers.PageSerializer


class PageView(APIView):
    def get(self, request, pk):
        try:
            page = Page.objects.get(pk=pk)
        except Page.DoesNotExist:
            raise Http404
        contents = []
        for content in page.contents.order_by('order_number'):
            serializer = serializers.content_serializers.get(type(content.content_object))
            if not serializer:
                continue
            contents.append(serializer(content.content_object).data)
        counter_inc.delay(page.id)
        return Response({
            'page': page.title,
            'content': contents,
        })
