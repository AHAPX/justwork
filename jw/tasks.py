from django.db import transaction

from jw.models import Page
from jw.celery import app

@app.task(name='counter_inc')
@transaction.atomic
def counter_inc(page_id):
    try:
        page = Page.objects.get(pk=page_id)
    except Page.DoesNotExist:
        return
    for content in page.contents.all():
        if content.content_object:
            content.content_object.counter += 1
            content.content_object.save()
